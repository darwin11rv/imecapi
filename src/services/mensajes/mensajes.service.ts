import { Injectable } from '@nestjs/common';
import { Mensaje } from 'src/controllers/mensajes/entyties/mensaje.entity';
import {InjectRepository} from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateMensajeDto } from 'src/controllers/mensajes/dto/create-mensaje-dto';

@Injectable()
export class MensajesService {
    constructor(
        @InjectRepository(Mensaje)
        private readonly mensajerepo:Repository<Mensaje>
    ){

    }

    async getAll(): Promise<Mensaje[]>{
        return await this.mensajerepo.find();
    }

    async createMensaje(mensajeN:CreateMensajeDto):Promise<Mensaje>{
        const msj=new Mensaje();
        msj.mensaje=mensajeN.mensaje;
        msj.nick=mensajeN.nick;
        return this.mensajerepo.save(msj);
    }
    async updateMensaje(idMensaje: number, mensajeActualzar:CreateMensajeDto):Promise<Mensaje>{
        const msjup=await this.mensajerepo.findOne(idMensaje);
        msjup.mensaje=mensajeActualzar.mensaje;
        msjup.nick=mensajeActualzar.nick;
        return this.mensajerepo.save(msjup);
    }

    async deleteMensaje(idMensaje:number):Promise<any>{
        return this.mensajerepo.delete(idMensaje)
    }

}
