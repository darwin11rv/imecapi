import { Controller, Get, Post, Body, Put, Delete, Res, HttpStatus, Param } from '@nestjs/common';
import { AppService } from '../../app.service'
import { CreateMensajeDto } from './dto/create-mensaje-dto';
import { MensajesService } from 'src/services/mensajes/mensajes.service';

/**
 * 
 */
@Controller('mensajes')
export class MensajesController {
    constructor(private readonly appService: AppService,
        private msjSer:MensajesService
        ) {

    }

    @Post()
    create(@Body() createMensajeDto:CreateMensajeDto, @Res() response){
        /** */
        this.msjSer.createMensaje(createMensajeDto).then(
            mensaje =>{ 
                response.status(HttpStatus.CREATED).json(mensaje);
            }
        ).catch(
            ()=>{
                response.status(HttpStatus.FORBIDDEN).json({mensaje :'Error en la creación del mensaje'});
            }
        );

    }

    @Get()
    GetMensajes(@Res() response){
        this.msjSer.getAll().then(
            mensajes =>{ 
                response.status(HttpStatus.OK).json(mensajes);
            }
        ).catch(
            ()=>{
                response.status(HttpStatus.FORBIDDEN).json({mensajes :'Error no se pudo obtener lso mensajes'});
            }
        );
    }

    @Put(':id')
    update(@Body() upadteMensajeDto:CreateMensajeDto, @Res() response, @Param('id') idMensaje){
        this.msjSer.updateMensaje(idMensaje,upadteMensajeDto).then(
            mensaje =>{ 
                response.status(HttpStatus.OK).json(mensaje);
            }
        ).catch(
            ()=>{
                response.status(HttpStatus.FORBIDDEN).json({mensaje :'Error en la actualización del mensaje'});
            }
        );
    }

    @Delete(':id')
    delete( @Res() response, @Param('id') idMensaje){
        this.msjSer.deleteMensaje(idMensaje).then(
            res =>{ 
                response.status(HttpStatus.OK).json(res);
            }
        ).catch(
            ()=>{
                response.status(HttpStatus.FORBIDDEN).json({mensaje :'Error en la eliminación del mensaje'});
            }
        );
    }

}
